﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCrystalViewerNew
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCrystalViewerNew))
        Me.mnuViewer = New System.Windows.Forms.MenuStrip()
        Me.SendByMailToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.rptViewer = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.mnuViewer.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuViewer
        '
        Me.mnuViewer.AllowDrop = True
        Me.mnuViewer.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SendByMailToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.mnuViewer.Location = New System.Drawing.Point(0, 0)
        Me.mnuViewer.Name = "mnuViewer"
        Me.mnuViewer.Size = New System.Drawing.Size(664, 24)
        Me.mnuViewer.TabIndex = 2
        Me.mnuViewer.Text = "MenuStrip1"
        '
        'SendByMailToolStripMenuItem
        '
        Me.SendByMailToolStripMenuItem.Enabled = False
        Me.SendByMailToolStripMenuItem.Name = "SendByMailToolStripMenuItem"
        Me.SendByMailToolStripMenuItem.Size = New System.Drawing.Size(98, 20)
        Me.SendByMailToolStripMenuItem.Text = "&Send by E-Mail"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.ExitToolStripMenuItem.Text = "&Exit"
        '
        'rptViewer
        '
        Me.rptViewer.ActiveViewIndex = -1
        Me.rptViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.rptViewer.Cursor = System.Windows.Forms.Cursors.Default
        Me.rptViewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rptViewer.Location = New System.Drawing.Point(0, 24)
        Me.rptViewer.Name = "rptViewer"
        Me.rptViewer.Size = New System.Drawing.Size(664, 438)
        Me.rptViewer.TabIndex = 1
        '
        'frmCrystalViewerNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(664, 462)
        Me.Controls.Add(Me.rptViewer)
        Me.Controls.Add(Me.mnuViewer)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCrystalViewerNew"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmCrystalViewerNew"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnuViewer.ResumeLayout(False)
        Me.mnuViewer.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuViewer As System.Windows.Forms.MenuStrip
    Friend WithEvents SendByMailToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents rptViewer As CrystalDecisions.Windows.Forms.CrystalReportViewer
End Class
