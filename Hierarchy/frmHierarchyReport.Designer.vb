﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHierarchyReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAgentsList = New System.Windows.Forms.Button()
        Me.btnAgentsHierarchy = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnAgentsList
        '
        Me.btnAgentsList.Location = New System.Drawing.Point(83, 55)
        Me.btnAgentsList.Name = "btnAgentsList"
        Me.btnAgentsList.Size = New System.Drawing.Size(180, 93)
        Me.btnAgentsList.TabIndex = 0
        Me.btnAgentsList.Text = "Agent's List"
        Me.btnAgentsList.UseVisualStyleBackColor = True
        '
        'btnAgentsHierarchy
        '
        Me.btnAgentsHierarchy.Location = New System.Drawing.Point(83, 180)
        Me.btnAgentsHierarchy.Name = "btnAgentsHierarchy"
        Me.btnAgentsHierarchy.Size = New System.Drawing.Size(180, 93)
        Me.btnAgentsHierarchy.TabIndex = 1
        Me.btnAgentsHierarchy.Text = "Agent's Hierarchy"
        Me.btnAgentsHierarchy.UseVisualStyleBackColor = True
        '
        'frmHierarchyReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(358, 318)
        Me.Controls.Add(Me.btnAgentsHierarchy)
        Me.Controls.Add(Me.btnAgentsList)
        Me.Name = "frmHierarchyReport"
        Me.Text = "Hierarchy Report"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnAgentsList As Button
    Friend WithEvents btnAgentsHierarchy As Button
End Class
