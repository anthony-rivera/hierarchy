﻿Imports Microsoft.Office.Interop.Outlook
Imports System.Windows.Forms

''' <summary>
''' Form to view Crystal Reports.
''' </summary>
Public Class frmCrystalViewerNew

    Dim objReport As CrystalDecisions.CrystalReports.Engine.ReportDocument
    Dim rptDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument
    Dim bShowRefreshButton, bShowCloseButton, bShowGroupTreeButton, bShowGroupTree As Boolean
    Dim Parameters As CrystalDecisions.Shared.ParameterFields

    ''' <summary>
    ''' Creates new Instance.
    ''' </summary>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    ''' <summary>
    ''' Creates new Instance based on Input parameters.
    ''' </summary>
    ''' <param name="ReportDocument">Crystal report object (CrystalDecisions.CrystalReports.Engine.ReportDocument)</param>
    ''' <param name="ShowRefreshButton">Indicates if the refresh button will be shown in the viewer.</param>
    ''' <param name="ShowCloseButton">Indicates if the close button will be shown in the viewer.</param>
    ''' <param name="ShowGroupTree">Indicates if the group tree will be shown in the viewer.</param>
    ''' <param name="ShowGroupTreeButton">Indicates if the show group tree button will be shown in the viewer.</param>
    ''' <param name="sFormCaption">Form caption text.</param>
    Public Sub New(ByVal ReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, _
                   ByVal ShowRefreshButton As Boolean, ByVal ShowCloseButton As Boolean, _
                   ByVal ShowGroupTree As Boolean, ByVal ShowGroupTreeButton As Boolean, _
                   ByVal sFormCaption As String)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

        objReport = ReportDocument
        bShowRefreshButton = ShowRefreshButton
        bShowCloseButton = ShowCloseButton
        bShowGroupTreeButton = ShowGroupTreeButton
        bShowGroupTree = ShowGroupTree

        SendByMailToolStripMenuItem.Visible = False
        Me.Text = sFormCaption

    End Sub

#Region "Form's Control Events"

    ''' <summary>
    ''' Set the report viewer ReportSource.
    ''' </summary>
    ''' <param name="sender">Sender object.</param>
    ''' <param name="e">Event arguments.</param>
    ''' <remarks></remarks>
    Private Sub frmCrystalViewerNew_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Cursor = Cursors.WaitCursor
        rptViewer.Cursor = Cursors.WaitCursor

        Try
            If objReport Is Nothing Then
                rptDocument.Refresh()
                rptViewer.ReportSource = rptDocument
            Else
                rptViewer.ShowRefreshButton = bShowRefreshButton
                rptViewer.ShowCloseButton = bShowCloseButton
                'rptViewer.DisplayGroupTree = bShowGroupTree
                If bShowGroupTree Then
                    rptViewer.DisplayGroupTree = True
                    'rptViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.GroupTree
                Else
                    'rptViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
                    rptViewer.DisplayGroupTree = False
                End If
                rptViewer.ShowGroupTreeButton = bShowGroupTreeButton

                rptViewer.ReportSource = objReport

            End If

            SendByMailToolStripMenuItem.Enabled = True
            rptViewer.Show()
            

        Catch ex As CrystalDecisions.Shared.CrystalReportsException
            MessageBox.Show("Error generating Report [frmCrystalViewerNew_Load]." & Environment.NewLine & Environment.NewLine & "Error Description: " & ex.Message, _
                "Crystal Reports Viewer", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch err As CrystalDecisions.CrystalReports.Engine.EngineException
            MessageBox.Show("Error generating Report [frmCrystalViewerNew_Load]." & Environment.NewLine & Environment.NewLine & "Error Description: " & err.Message, _
                "Crystal Reports Viewer", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Catch ex1 As CrystalDecisions.ReportSource.EnterpriseException
            MessageBox.Show("Error generating Report [frmCrystalViewerNew_Load]." & Environment.NewLine & Environment.NewLine & "Error Description: " & ex1.Message, _
                "Crystal Reports Viewer", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            rptViewer.Cursor = Cursors.Default
            Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Export the report to a .pdf file and opens a new outolook message with the created file attached.
    ''' </summary>
    ''' <param name="sender">Sender object.</param>
    ''' <param name="e">Event arguments.</param>
    ''' <remarks></remarks>
    Private Sub SendByMailToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SendByMailToolStripMenuItem.Click
        'Dim userPath As String
        'Dim sAttachment As String

        'Try
        '    userPath = System.Environment.GetEnvironmentVariable("USERPROFILE").Trim.ToString
        '    sAttachment = userPath & "\" & _
        '                  objReport.FileName.Substring(objReport.FileName.LastIndexOf("\") + 1, (objReport.FileName.LastIndexOf(".") - 1) - objReport.FileName.LastIndexOf("\")) & ".pdf"

        '    Me.Cursor = Cursors.WaitCursor

        '    objReport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, sAttachment)

        '    'create application
        '    Dim oOutlook As New Microsoft.Office.Interop.Outlook.Application

        '    'connect to current namespace
        '    Dim oNamespace As Microsoft.Office.Interop.Outlook.NameSpace = oOutlook.GetNamespace("MAPI")
        '    oNamespace.Logon()

        '    'create a new email object
        '    Dim mi As MailItem = oOutlook.CreateItem(OlItemType.olMailItem)
        '    'mi.To = "@zzz.com"
        '    'mi.Body = "body text here"
        '    mi.Subject = ""

        '    'add attachments
        '    mi.Attachments.Add(sAttachment)

        '    'pop up email dialog
        '    mi.Display()

        '    'Delete the .pdf file
        '    Dim fiFile As New System.IO.FileInfo(sAttachment)
        '    fiFile.Delete()

        '    Me.Cursor = Cursors.Default

        'Catch ex As System.Exception
        '    Me.Cursor = Cursors.Default
        '    MessageBox.Show(ex.Message, "View Reports", MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub

    ''' <summary>
    ''' Closes the viewer form.
    ''' </summary>
    ''' <param name="sender">Sender object.</param>
    ''' <param name="e">Event arguments</param>
    ''' <remarks></remarks>
    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

#End Region

#Region "Form Funcitons and Subs"



#End Region

End Class