﻿Public Class Report
    Inherits CrystalDecisions.CrystalReports.Engine.ReportDocument

#Region "Properties"
    Public sDSN As String = System.Configuration.ConfigurationSettings.AppSettings("CrystalServerName")
    Public dataBaseName As String = System.Configuration.ConfigurationSettings.AppSettings("CrystalDatabaseName")
    Public sLogonUser As String = System.Configuration.ConfigurationSettings.AppSettings("CrystalUserID")
    Public sPwd As String = System.Configuration.ConfigurationSettings.AppSettings("CrystalPassword")
#End Region

#Region "Methods"
    Public Sub GenerateAndDisplayReportViewer(Of T)(ByVal reportName As String, ByVal recordSelectionFormula As String, parameterFields As List(Of T))
        Me.Load(reportName)

        'For i As Integer = 0 To Me.Database.Tables.Count - 1
        '    Me.Database.Tables(i).LogOnInfo.ConnectionInfo.IntegratedSecurity = True
        'Next

        For i As Integer = 0 To Me.Database.Tables.Count - 1
            Me.Database.Tables(i).LogOnInfo.ConnectionInfo.ServerName = sDSN
            Me.Database.Tables(i).LogOnInfo.ConnectionInfo.DatabaseName = dataBaseName
            Me.Database.Tables(i).LogOnInfo.ConnectionInfo.UserID = sLogonUser
            Me.Database.Tables(i).LogOnInfo.ConnectionInfo.Password = sPwd
        Next

        Me.SetDatabaseLogon(sLogonUser, sPwd)

        If recordSelectionFormula <> "" Then
            Me.RecordSelectionFormula = recordSelectionFormula
        End If

        If Not IsNothing(parameterFields) Then
            For count As Integer = 0 To parameterFields.Count - 1
                Me.ParameterFields.Item(count).CurrentValues.AddValue(parameterFields(count))
            Next
        End If

        Dim frmReportViewer As New frmCrystalViewerNew(Me, True, True, True, True, "")
        frmReportViewer.ShowDialog()
    End Sub

    ''Public Sub GenerateAndDisplayReportViewer(Of T)(ByVal reportName As String, ByVal recordSelectionFormula As String, parameterFields As List(Of T), ByVal reportTitle As String)
    ''    Me.Load(reportName)

    ''    For i As Integer = 0 To Me.Database.Tables.Count - 1
    ''        Me.Database.Tables(i).LogOnInfo.ConnectionInfo.IntegratedSecurity = True
    ''    Next

    ''    If recordSelectionFormula <> "" Then
    ''        Me.RecordSelectionFormula = recordSelectionFormula
    ''    End If

    ''    If Not IsNothing(parameterFields) Then
    ''        For count As Integer = 0 To parameterFields.Count - 1
    ''            Me.ParameterFields.Item(count).CurrentValues.AddValue(parameterFields(count))
    ''        Next
    ''    End If

    ''    Dim frmReportViewer As New frmCrystalViewerNew(Me, True, True, True, True, reportTitle)
    ''    frmReportViewer.ShowDialog()
    ''End Sub

    Public Sub GenerateDisplayOrDirectPrintWithFormulaFields(Of T)(reportName As String, parameterFields As List(Of T), formulaFields As List(Of String), reportTitle As String, printPreview As Boolean, Optional ByVal recordSelectionFormula As String = "")
        Me.Load(reportName)
        ''Me.Refresh()

        If printPreview = True Then
            For i As Integer = 0 To Me.Database.Tables.Count - 1
                Me.Database.Tables(i).LogOnInfo.ConnectionInfo.IntegratedSecurity = True
            Next
        Else
            For i As Integer = 0 To Me.Database.Tables.Count - 1
                Me.Database.Tables(i).LogOnInfo.ConnectionInfo.ServerName = sDSN
                Me.Database.Tables(i).LogOnInfo.ConnectionInfo.DatabaseName = dataBaseName
                Me.Database.Tables(i).LogOnInfo.ConnectionInfo.UserID = sLogonUser
                Me.Database.Tables(i).LogOnInfo.ConnectionInfo.Password = sPwd
            Next

            Me.SetDatabaseLogon(sLogonUser, sPwd)
        End If

        If Not IsNothing(formulaFields) Then
            For count As Integer = 0 To formulaFields.Count - 1
                Me.DataDefinition.FormulaFields.Item(count).Text = "'" & formulaFields(count) & "'"
            Next
        End If

        If Not IsNothing(parameterFields) Then
            For count As Integer = 0 To parameterFields.Count - 1
                Me.ParameterFields.Item(count).CurrentValues.AddValue(parameterFields(count))
            Next
        End If

        If recordSelectionFormula <> "" Then
            Me.RecordSelectionFormula = recordSelectionFormula
        End If

        If printPreview = True Then
            Dim frmReportViewer As New frmCrystalViewerNew(Me, True, True, True, True, reportTitle)
            frmReportViewer.ShowDialog()
        Else
            Me.PrintToPrinter(0, False, 0, 0)
        End If
    End Sub

    Public Sub GenerateReportDirecToPrinter(Of T)(ByVal reportName As String, ByVal recordSelectionFormula As String, parameterFields As List(Of T), ByVal reportTitle As String)
        Me.Load(reportName)

        For i As Integer = 0 To Me.Database.Tables.Count - 1
            Me.Database.Tables(i).LogOnInfo.ConnectionInfo.ServerName = sDSN
            Me.Database.Tables(i).LogOnInfo.ConnectionInfo.DatabaseName = dataBaseName
            Me.Database.Tables(i).LogOnInfo.ConnectionInfo.UserID = sLogonUser
            Me.Database.Tables(i).LogOnInfo.ConnectionInfo.Password = sPwd
        Next

        Me.SetDatabaseLogon(sLogonUser, sPwd)

        If recordSelectionFormula <> "" Then
            Me.RecordSelectionFormula = recordSelectionFormula
        End If

        If Not IsNothing(parameterFields) Then
            For count As Integer = 0 To parameterFields.Count - 1
                Me.ParameterFields.Item(count).CurrentValues.AddValue(parameterFields(count))
            Next
        End If

        Me.PrintToPrinter(0, False, 0, 0)
    End Sub

    Public Sub GenerateReportDirecToPrinterIntegratedSecurity(Of T)(ByVal reportName As String, ByVal recordSelectionFormula As String, parameterFields As List(Of T), ByVal reportTitle As String)
        Me.Load(reportName)

        For i As Integer = 0 To Me.Database.Tables.Count - 1
            Me.Database.Tables(i).LogOnInfo.ConnectionInfo.IntegratedSecurity = True
        Next

        If recordSelectionFormula <> "" Then
            Me.RecordSelectionFormula = recordSelectionFormula
        End If

        If Not IsNothing(parameterFields) Then
            For count As Integer = 0 To parameterFields.Count - 1
                Me.ParameterFields.Item(count).CurrentValues.AddValue(parameterFields(count))
            Next
        End If

        Me.PrintToPrinter(0, False, 0, 0)
    End Sub

#End Region
End Class
