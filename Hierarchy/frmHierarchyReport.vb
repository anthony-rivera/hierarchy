﻿Public Class frmHierarchyReport
    Private Sub btnAgentsList_Click(sender As Object, e As EventArgs) Handles btnAgentsList.Click
        Dim newReport As New Report
        Dim emptyParameterList As List(Of Object) = Nothing


        newReport.GenerateAndDisplayReportViewer(Application.StartupPath & "\Reports\HierarchyReport.rpt", String.Empty, emptyParameterList)
        newReport.Close()
        newReport.Dispose()
        ''newReport.GenerateAndDisplayReportViewer(Application.StartupPath & "\Reports\Hierarchy-ProdEN15-gadev-LPSQL.rpt", String.Empty, emptyParameterList)

    End Sub

    Private Sub btnAgentsHierarchy_Click(sender As Object, e As EventArgs) Handles btnAgentsHierarchy.Click
        Dim newReport As New Report
        Dim emptyParameterList As List(Of Object) = Nothing


        newReport.GenerateAndDisplayReportViewer(Application.StartupPath & "\Reports\HierarchyReport-Structured.rpt", String.Empty, emptyParameterList)
        newReport.Close()
        newReport.Dispose()
    End Sub
End Class